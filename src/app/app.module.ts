import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HomeComponent } from './homes/home/home/home.component';
import { LoginComponent } from './homes/login/login/login.component';
import { ProductComponent } from './homes/product/product/product.component';
import { CartComponent } from './homes/cart/cart/cart.component';
import { CheckoutComponent } from './homes/checkout/checkout/checkout.component';
import { CustomerInfoComponent } from './homes/checkout/customer-info/customer-info.component';
import { PaymentInfoComponent } from './homes/checkout/payment-info/payment-info.component';
import { ReceiptInfoComponent } from './homes/checkout/receipt-info/receipt-info.component';
import { ItemComponent } from './homes/product/item/item.component';
import { HttpClientModule } from '@angular/common/http';
import { RegisterComponent } from './homes/register/register.component';
import { LogoutComponent } from './homes/logout/logout.component';
import { AppGuard } from './app.guard';
import { AuthService } from './service/auth.service';
import { AdminComponent } from './homes/admin/admin.component';
import { AdminGuard } from './admin.guard';
import { OrderDetailComponent } from './homes/admin/order-detail/order-detail.component';
import { UserDetailComponent } from './homes/admin/user-detail/user-detail.component';
import { OrderByUserComponent } from './homes/admin/userDetail/order-by-user/order-by-user.component';
import { OrderHistoryComponent } from './homes/order-history/order-history.component';
import { OrderChildComponent } from './homes/order-history/order-child/order-child.component';
import { ItemChildComponent } from './homes/order-history/orderChild/item-child/item-child.component';
import { CheckoutChildComponent } from './homes/checkout/checkout-child/checkout-child.component';
import { ItemByOrderComponent } from './homes/admin/userDetail/order-by-user/item-by-order/item-by-order.component';
import { EditProductComponent } from './homes/admin/edit-product/edit-product.component';
import { EditItemComponent } from './homes/admin/editProduct/edit-item/edit-item.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    ProductComponent,
    CartComponent,
    CheckoutComponent,
    CustomerInfoComponent,
    PaymentInfoComponent,
    ReceiptInfoComponent,
    ItemComponent,
    RegisterComponent,
    LogoutComponent,
    AdminComponent,
    OrderDetailComponent,
    UserDetailComponent,
    OrderByUserComponent,
    OrderHistoryComponent,
    OrderChildComponent,
    ItemChildComponent,
    CheckoutChildComponent,
    ItemByOrderComponent,
    EditProductComponent,
    EditItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    AppGuard,
    AdminGuard,
    AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
