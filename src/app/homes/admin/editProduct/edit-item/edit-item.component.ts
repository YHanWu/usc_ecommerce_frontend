//@ts-nocheck
import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AdminService } from 'src/app/service/admin.service';

@Component({
  selector: 'app-edit-item',
  templateUrl: './edit-item.component.html',
  styleUrls: ['./edit-item.component.css']
})
export class EditItemComponent implements OnInit {

  @Input
  item;

  newObj: Object;

  prodctItem: Object;
  subcription:any;


  myFormGroup: FormGroup;
  

  constructor(private adminService: AdminService) { 
    this.myFormGroup = new FormGroup({
      price: new FormControl(),
      status: new FormControl()      
    })
  }

  ngOnInit(): void {

    this.prodctItem = Object.assign({}, this.item);
    this.prodctItem = Object.assign({quantity: 1}, this.prodctItem);


  }

  edit(){
    //console.log(this.myFormGroup.value)
    this.newObj = Object.assign(this.item, this.myFormGroup.value);
    //console.log(this.newObj);
    this.adminService.editProduct(this.newObj).subscribe(res =>{
      console.log(res);
    })

    
  }


}
