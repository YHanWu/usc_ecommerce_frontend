import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AdminService } from 'src/app/service/admin.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  orderSubject: any;
  addSubject: any;
  userSubject: any;

  orderResponse: any;
  userResponse: any;

  ordertableController: boolean = false;
  addProductController: boolean = false;
  getUserController: boolean = false;


  myFormGroup: FormGroup;

  constructor(private adminService: AdminService, private router: Router) { this.myFormGroup = new FormGroup({
    item: new FormControl(),
    brand: new FormControl(),
    category: new FormControl(),
    type: new FormControl(),
    country: new FormControl(),
    size: new FormControl(),
    region: new FormControl(),
    status: new FormControl(),
    url: new FormControl(),
    price: new FormControl(),
    taste: new FormControl()
  });}

  ngOnInit(): void {
  }


  getOrders(){
    this.addProductController = false;
    this.getUserController = false;
    this.ordertableController=true;
    this.orderSubject = this.adminService.checkOrder().subscribe(
      res =>{
        this.orderResponse = res.value;
      }
    )
  }

  getUsers(){
    this.ordertableController =false;
    this.addProductController = false;
    this.getUserController = true;
    // this.orderSubject.unsubscribe();
    // this.addSubject .unsubscribe();
    this.userSubject = this.adminService.getUser().subscribe(
      res => {
        this.userResponse = res.value;
        //this.userResponse = res;
      }
    )
    

  }

  addProduct(){
    this.ordertableController =false;
    this.getUserController = false;
    this.addProductController = true;
    //this.orderSubject.unsubscribe();
    

  }

  onSubmit(){
    console.log(this.myFormGroup.value);
    this.addSubject = this.adminService.addProduct(this.myFormGroup.value).subscribe(
      (res) =>{
        //this.router.navigate(['/amdin']);
      }
    )
    this.addProductController = false;
  }


  editProduct(){
    this.router.navigate(['/edit']);

  }

}
