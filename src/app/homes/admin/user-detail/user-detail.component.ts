//@ts-nocheck
import { Component, Input, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AdminService } from 'src/app/service/admin.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {

  @Input
  user: any;

  orderById: any;
  clickController: boolean = false;
  subscription: Subscription;

  constructor(private adminService: AdminService) { }

  ngOnInit(): void {
  }

  getOrdersbyUser(){
    this.clickController = true;
    this.subscription = this.adminService.getOrdersById(this.user.id).subscribe(
      res =>{
        this.orderById = res.value;
      }
    );
    //this.subscription.unsubscribe();
    this.subscription.remove();
  }

}
