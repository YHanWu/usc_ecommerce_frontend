//@ts-nocheck
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-item-by-order',
  templateUrl: './item-by-order.component.html',
  styleUrls: ['./item-by-order.component.css']
})
export class ItemByOrderComponent implements OnInit {

  @Input
  item;

  constructor() { }

  ngOnInit(): void {
  }

}
