import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemByOrderComponent } from './item-by-order.component';

describe('ItemByOrderComponent', () => {
  let component: ItemByOrderComponent;
  let fixture: ComponentFixture<ItemByOrderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemByOrderComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ItemByOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
