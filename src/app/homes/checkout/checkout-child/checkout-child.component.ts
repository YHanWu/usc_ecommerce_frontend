//@ts-nocheck
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-checkout-child',
  templateUrl: './checkout-child.component.html',
  styleUrls: ['./checkout-child.component.css']
})
export class CheckoutChildComponent implements OnInit {

  @Input
  item;

  constructor() { }

  ngOnInit(): void {
  }

}
