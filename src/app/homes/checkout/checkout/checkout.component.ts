import { Component, OnInit } from '@angular/core';
import { ShoppingCartService } from 'src/app/service/shopping-cart.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {

  orders: any;

  constructor(private shoppingCartService: ShoppingCartService) { }

  ngOnInit(): void {
    this.orders = this.shoppingCartService.orderList;
  }

}
