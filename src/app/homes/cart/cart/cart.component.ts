//@ts-nocheck
import { Component, DoCheck, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { Subject, Subscription, takeUntil } from 'rxjs';
import { FormsModule } from '@angular/forms';


import { ShoppingCartService } from 'src/app/service/shopping-cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})

export class CartComponent implements OnInit, OnDestroy, DoCheck {

  // public selectedProduct: Map<any, number> = new Map<Product, number>();
  // private ngUnsubscribe: Subject<void> = new Subject<void>();


  products : any =[];
  newproducts : any=[];
  productsQuantity = new Map<string , number>();
  grandTotal : number = 0;
  totalprice: number = 0;
  order ={ money: 0, address: "", orderitems: []};
  address = "";
  item ={num: 0, item: {id: 0}};
  subscription: Subject = new Subject<any>();
  
  constructor(private shoppingCartService: ShoppingCartService) { }
  

  ngOnInit(): void {
    this.subscription = this.shoppingCartService.getProduct()
    .subscribe(res=>{
      this.products = res;      
      this.newproducts =[];
      this.productsQuantity = new Map<string , number>();

      for(let product of this.products){
        if(!this.productsQuantity.has(product.id)){
          this.productsQuantity.set(product.id, 1);
        }else{
          this.productsQuantity.set(product.id, this.productsQuantity.get(product.id)+1)
        }
      }

      for(let key of this.productsQuantity.keys()){
        for(let product of this.products){
          if(product.id == key){
            console.log(product);
            this.newproducts.push(Object.assign(product, {quantity: this.productsQuantity.get(key)}));
            break;
          }
        }
      }


      console.log(this.newproducts);

    })

    let totalMoney=0;
    for(let product of this.newproducts){
      totalMoney += product.quantity*product.price;
      
    }
    this.grandTotal = totalMoney;
  }

  ngDoCheck(): void {
    let totalMoney=0;
    for(let product of this.newproducts){
      totalMoney += product.quantity*product.price;
      
    }
    this.grandTotal = totalMoney;
  }

  placeOrder(){
    let totalMoney=0;
    for(let product of this.newproducts){
      totalMoney += product.quantity*product.price;
      
    }
    
    this.order.money = totalMoney;
    this.order.address = this.address;
    for(let i=0; i< this.newproducts.length; i++){
      this.item ={num: 0, item: {id: 0}};
      this.item.num = this.newproducts[i].quantity;
      console.log(this.newproducts[i].id);
      this.item.item.id = this.newproducts[i].id ;    
      this.order.orderitems.push(Object.assign({}, this.item));
    }

    this.shoppingCartService.orderCreate(this.order).subscribe((res)=>{

    });
    this.subscription.unsubscribe(); 
    


    //remeber to erase everything after order placed
    

  }

  remove(id){
    this.newproducts.forEach( (product, index) => {
      if(product.id == id) this.newproducts.splice(index, 1);
    })
  }


  ngOnDestroy() {
    this.subscription.unsubscribe();
}

  


}
