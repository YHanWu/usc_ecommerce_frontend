//@ts-nocheck
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  response;

  constructor(public authService: AuthService,
    private router: Router) { }

  ngOnInit(): void {
    this.authService.checklogin()
    .subscribe((res) => {   //res is what observable emitted after execution
      this.response = res;
      console.log(res);
      if(res.success){
        this.router.navigate(['/']);
      }
    });
  }



  login(user){
    this.authService.login(user)
    .subscribe((res) => {
      this.response= res;
      console.log(this.response);
      if(res.message == "Admin"){
        this.router.navigate(['/admin']);
      }
    });
  } 


}
