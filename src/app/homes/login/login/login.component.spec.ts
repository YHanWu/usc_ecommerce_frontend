import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { AuthService } from 'src/app/service/auth.service';

import { LoginComponent } from './login.component';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let authServiceMock: any;
  let routerMock: any;

  beforeEach(async () => {
    authServiceMock = {
      login: jest.fn()
    }
    routerMock = {
      navigate: jest.fn()
    }
    await TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      providers: [
        {
          provide: AuthService, useValue: authServiceMock
        },
        {
          provide: Router, useValue: routerMock
        }
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should login', () => {
    const expRes = {
      success: true,
      code: 200,
      message: "Admin"
    };
    jest.spyOn(authServiceMock, 'login').mockReturnValue(of(expRes));
    fixture.detectChanges();
    expect(component.response.code).toBe(expRes.code);

  });
});
