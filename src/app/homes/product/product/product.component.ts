//@ts-nocheck
import { HttpErrorResponse } from '@angular/common/http';
import { Component, DoCheck, OnInit } from '@angular/core';
import { catchError, Observable, of, startWith } from 'rxjs';
import { ProductService } from 'src/app/service/product.service';
import { map } from 'rxjs/operators';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit, DoCheck {

  public productList: any;
  sortCondition = '';

  productState$: Observable<{ appState: any, appData?: any, error?: HttpErrorResponse}>;

  constructor(private productService: ProductService ) { }

  ngOnInit(): void {
    this.productState$ = this.productService.product$().pipe(
      map((response) => {
        console.log(response);
        this.productList = response;
        return({appState: 'APP_LOADED', appData: response});
      }
    ),
    startWith({appState: 'APP_LOADING'}),
    catchError((error: HttpErrorResponse) => of({ appState: 'APP_ERROR', error }))
    )

    // this.productService.getProduct()
    // .subscribe(
    //   (response) => this.newProduct = response,
    //   error => console.log(error),
    //   () => {
    //     return console.log('Done retrieving product');
    //   });

    // console.log(this.newProduct[0].id);
  }
  ngDoCheck(): void {
    
  }

  SortbyPrice(){
    this.productState$ = this.productService.product$('price').pipe(
      map((response) => {
        console.log(response);
        this.productList = response;
        return({appState: 'APP_LOADED', appData: response});
      }
    ),
    startWith({appState: 'APP_LOADING'}),
    catchError((error: HttpErrorResponse) => of({ appState: 'APP_ERROR', error }))
    )
  }

  SortbyBrand(){
    this.productState$ = this.productService.product$('brand').pipe(
      map((response) => {
        console.log(response);
        this.productList = response;
        return({appState: 'APP_LOADED', appData: response});
      }
    ),
    startWith({appState: 'APP_LOADING'}),
    catchError((error: HttpErrorResponse) => of({ appState: 'APP_ERROR', error }))
    )
  }



}
