//@ts-nocheck
import { Component, Input, OnInit } from '@angular/core';
import { Product } from 'src/app/interface/product';
import { ShoppingCartService } from 'src/app/service/shopping-cart.service';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {

  @Input
  item;

  prodctItem: Object;
  subcription:any;
  

  constructor(private shoppingCartService: ShoppingCartService) { }

  ngOnInit(): void {
    //Object.assign(this.item, {quantity: 1, total: parseInt(this.item.price)});
    this.prodctItem = Object.assign({}, this.item);
    // console.log(this.prodctItem);
    // console.log(this.item);
    this.prodctItem = Object.assign({quantity: 1}, this.prodctItem);

    // console.log(this.prodctItem);
    // console.log(this.item);
  }

  addProductToCart(){
    //console.log("adding item" + this.item.item);
    this.subcription = this.shoppingCartService.addSelectedProduct(this.item);
    console.log("add item success" + this.item.item);
  }

  addtoCart(){
    // console.log("addtoCart funciton!!!!!!!!");
    // console.log(this.item);
    this.shoppingCartService.addtoCart(this.prodctItem);
  }

}
