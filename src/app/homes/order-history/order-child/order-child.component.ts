//@ts-nocheck
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-order-child',
  templateUrl: './order-child.component.html',
  styleUrls: ['./order-child.component.css']
})
export class OrderChildComponent implements OnInit {
  @Input
  order;

  constructor() { }

  ngOnInit(): void {
  }

}
