//@ts-nocheck
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-item-child',
  templateUrl: './item-child.component.html',
  styleUrls: ['./item-child.component.css']
})
export class ItemChildComponent implements OnInit {
  @Input
  item;

  constructor() { }

  ngOnInit(): void {
  }

}
