//@ts-nocheck
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ShoppingCartService } from 'src/app/service/shopping-cart.service';

@Component({
  selector: 'app-order-history',
  templateUrl: './order-history.component.html',
  styleUrls: ['./order-history.component.css']
})
export class OrderHistoryComponent implements OnInit {

  subscription: Subscription;

  history: any;

  constructor(private shoppingCartService: ShoppingCartService) { }

  ngOnInit(): void {
    this.subscription =this.shoppingCartService.getOrderHistory().subscribe(
      res => {
        this.history = res;
      }
    )
    
  }

}
