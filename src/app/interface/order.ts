import { Item } from "./itemobj";

export interface Order {
    money: number,
    address: String,
    orderitems: [Item]
}