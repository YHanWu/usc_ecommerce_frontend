export interface User {
    id: number,
    username: String,
    password: String,
    userRole: any,
    enabled: boolean,
    accountNonExpired: boolean,
    credentialsNonExpired: boolean,
    authorities: any,
    userName: String,
    accountNonLocked: boolean
}