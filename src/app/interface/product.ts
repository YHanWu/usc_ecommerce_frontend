export interface Product {
    id: number,
    item: string,
    brand: string,
    category: string,
    country: string,
    type: string,
    size: string,
    region: string,
    status: string,
    url: string,
    price: number,
    taste: string;
}