import { registerLocaleData } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule, Routes} from '@angular/router';
import { AdminGuard } from "./admin.guard";
import { AppGuard } from "./app.guard";
import { AdminComponent } from "./homes/admin/admin.component";
import { EditProductComponent } from "./homes/admin/edit-product/edit-product.component";
import { CartComponent } from "./homes/cart/cart/cart.component";
import { CheckoutComponent } from "./homes/checkout/checkout/checkout.component";
import { HomeComponent } from "./homes/home/home/home.component";
import { LoginComponent } from "./homes/login/login/login.component";
import { LogoutComponent } from "./homes/logout/logout.component";
import { OrderHistoryComponent } from "./homes/order-history/order-history.component";
import { ProductComponent } from "./homes/product/product/product.component";
import { RegisterComponent } from "./homes/register/register.component";


const routes: Routes=[
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
    },
    {
        path: '',
        canActivate: [AppGuard],
        children:[
            {
                path: 'home',
                component: HomeComponent
            },
            {
                path: 'product',
                component: ProductComponent
            },
            {
                path: 'cart',
                component: CartComponent
            },
            {
                path: 'checkout',
                component: CheckoutComponent
            },
            {
                path: 'logout',
                component: LogoutComponent
            },
            {
                path: 'checkout',
                component: CheckoutComponent
            },
            {
                path: 'history',
                component: OrderHistoryComponent
            }   
        ]
    },
    {
        path: '',
        canActivate: [AdminGuard],
        children:[
            {
            path: 'admin',
            component: AdminComponent
            },
            {
                path: 'edit',
                component: EditProductComponent
                }
        ]

    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'register',
        component: RegisterComponent
    }

    
]

@NgModule({
    imports:[RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule{}