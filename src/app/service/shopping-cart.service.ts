//@ts-nocheck
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, of, Subject, ReplaySubject, map, pipe, Observable } from 'rxjs';
// import { environment } from 'src/environments/environment';
import { Product } from '../interface/product';


@Injectable({
  providedIn: 'root'
})
export class ShoppingCartService {

  //private API_URL = environment.API_URL;
  //private API_URL= 'http://localhost:8080';
  private API_URL= 'http://44.201.19.235:8080';

  private selectedProduct: Map<any, number> = new Map<Product, number>();
  public productList = new BehaviorSubject<any>([]);
  public orderList = new BehaviorSubject<any>();

  //public cartItemList : any = [];
  public cartItemList = [];

  constructor(private http: HttpClient, private router: Router){};

  //getter
  getProduct(){
    return this.productList.asObservable();
  }

  //setter
  setProduct(product: any){
    this.cartItemList.push(...product);
    this.productList.next(product);
  }

  addtoCart(product: any){
    this.cartItemList.push(product);
    this.productList.next(this.cartItemList);
    this.getToatalPrice();
    console.log(this.cartItemList);
  }

  orderCreate(order): Observable<any> {
    return this.http.post(this.API_URL + "/order/create", order, { withCredentials: true })
      .pipe(map(res => { console.log(res);
        this.orderList = res;
        this.productList.next([]);
        this.cartItemList = [];
        this.router.navigate(['/checkout']);
      }));
  }

  getOrderHistory(): Observable<any> {
    return this.http.get(this.API_URL + "/order/orderItems", { withCredentials: true })
    .pipe(map(res => {
      console.log(res);
      return res;
    }));
  }


  getToatalPrice(): number{
    let priceTotal: number = 0;
    this.cartItemList.map((a: number) => {
      priceTotal += a.total;
    })
    return priceTotal;
  }

  removeCartItem(product: any){
    this.cartItemList.map((a: any, index:any) => {
      if(product.id === a.id){
        this.cartItemList.splice(index, 1);
      }
    })
  }

  removeAllCart(){
    this.cartItemList = [];
    this.productList.next(this.cartItemList);
  }

}
