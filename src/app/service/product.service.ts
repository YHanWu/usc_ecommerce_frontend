import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { BehaviorSubject, Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ProductService {

  //private readonly serverUrl: string = 'http://localhost:8080';
  private readonly serverUrl: string = 'http://44.201.19.235:8080';

  constructor(private http: HttpClient) {   }

  

  //call the back end and retriev the data
  // getProduct(name: string = '', page: number = 0, size: number = 10): Observable<any>{
  //   return this.http.get<any>(`${this.serverUrl}/product?name=${name}&page=${page}&size=${size}`);
  // }


  //call the back end and retriev the data, another appoarch
  product$ = (name: string = '', page: number = 0, size: number = 8): Observable<any> =>
    this.http.get<any>(`${this.serverUrl}/product?name=${name}&page=${page}&size=${size}`);
    


}
