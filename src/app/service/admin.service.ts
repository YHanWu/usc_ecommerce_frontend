//@ts-nocheck
import { Injectable } from '@angular/core';
// import { environment } from 'src/environments/environment';
import { Subject, BehaviorSubject, Observable, pipe } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient, HttpParams } from '@angular/common/http';
// import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  // private API_URL = environment.API_URL;
  //private API_URL = 'http://localhost:8080';
  private API_URL = 'http://44.201.19.235:8080';

  Order: Subject<any> = new BehaviorSubject<any>();
  User: Subject<any> = new BehaviorSubject<any>();
  OrderByUser: Subject<any> = new BehaviorSubject<any>();

  constructor(private http: HttpClient) { }


  checkOrder(): Observable<any> {
    return this.http.get(this.API_URL + "/orders", { withCredentials: true })   // {withCredentials:true} means add JssessionID stored in the cookie as part of request
      .pipe(map((res) => {
        this.Order.next(res);
        //console.log(this.Order);
        return this.Order;
      }));
  }

  getOrdersById(id: any): Observable<any> {
    return this.http.get(this.API_URL + "/order/" + id + "/orderItems", { withCredentials: true })   // {withCredentials:true} means add JssessionID stored in the cookie as part of request
      .pipe(map((res) => {
        this.OrderByUser.next(res);
        //console.log(this.Order);
        return this.OrderByUser;
      }));
  }

  getUser(): Observable<any> {
    return this.http.get(this.API_URL + "/users/users", { withCredentials: true })   // {withCredentials:true} means add JssessionID stored in the cookie as part of request
      .pipe(map((res) => {
        this.User.next(res);
        console.log(this.User);
        return this.User;
      }));
  }

  addProduct(product): Observable<any> {
    return this.http.post(this.API_URL + "/product", product, { withCredentials: true })   // {withCredentials:true} means add JssessionID stored in the cookie as part of request
      .pipe(map((res) => {
        
        
      }));
  }

  editProduct(product): Observable<any> {
    return this.http.put(this.API_URL + "/product/edit", product, { withCredentials: true })   // {withCredentials:true} means add JssessionID stored in the cookie as part of request
      .pipe(map((res) => {
        
        return res;
      }));
  }
}
