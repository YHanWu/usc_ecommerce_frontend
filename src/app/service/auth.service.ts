//@ts-nocheck
import { Injectable } from '@angular/core';
// import { environment } from 'src/environments/environment';
import { Subject, BehaviorSubject, Observable, pipe } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  // private API_URL = environment.API_URL;
  //private API_URL= 'http://localhost:8080';
  private API_URL= 'http://44.201.19.235:8080';

  loggedIn: Subject<boolean> = new BehaviorSubject<boolean>(false);
  Admin: Subject<boolean> = new BehaviorSubject<boolean>(false);
  

  constructor(private http: HttpClient, private router: Router) { }

  login(user): Observable<any> {
    let params = new HttpParams();
    params = params.append("username", user.username);
    params = params.append("password", user.password);
    return this.http.post(this.API_URL + "/login", params, {withCredentials: true})
      .pipe(map((res) => {
        this.loggedIn.next(res.success);
        if(res.message == "Admin"){
          this.Admin.next(true);
        }
        if (this.loggedIn) {
          this.router.navigate(['/product']);
        }
        return res;
      }));
  }

  checklogin(): Observable<any> {
    return this.http.get(this.API_URL + "/checklogin", { withCredentials: true })   // {withCredentials:true} means add JssessionID stored in the cookie as part of request
      .pipe(map((res) => {
        this.loggedIn.next(res.success);
        return res;
      }));
  }

  logout(): Observable<any> {
    return this.http.post(this.API_URL + "/logout", {}, { withCredentials: true })
      .pipe(map(res => {
        res;
        this.loggedIn.next(false);
        this.router.navigate(['/login']);
        return res;
      }));
  }

  register(user): Observable<any> {
    return this.http.post(this.API_URL + "/users", user)
      .pipe(map(res => {
        console.log(res);
        if (res.success) {
          this.router.navigate(['/login']);
        }
      }));
  }
}
