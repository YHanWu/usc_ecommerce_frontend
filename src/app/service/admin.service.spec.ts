

import { of } from 'rxjs';
import { User } from '../interface/user';
import { AdminService } from './admin.service';

describe('AdminService', () => {
  let service: AdminService;
  let httpClientSpy: any;
  const user: User = {
    id: 1,
    username: "test",
    password: "testPassword",
    userRole: [
      {
          "id": 1,
          "type": "ROLE_ADMIN",
          "authority": "ROLE_ADMIN"
      }
  ],
    enabled: true,
    accountNonExpired: true,
    credentialsNonExpired: true,
    authorities: [
      {
          "id": 1,
          "type": "ROLE_ADMIN",
          "authority": "ROLE_ADMIN"
      }
  ],
    userName: "test",
    accountNonLocked: true
  }

  beforeEach(() => {
    httpClientSpy = {
      get: jest.fn()
    }
    service = new AdminService(httpClientSpy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should test get User', (done) => {
    
    const res = [user];
    const url = "http://44.201.19.235:8080/users/users";

    jest.spyOn(httpClientSpy, 'get').mockReturnValue(of(res));
    service.getUser().subscribe(
      {
        next: data => {
          expect(data.value).toEqual(res);
          done();
        },
        error: error => console.log(error)
      }
    )
    // expect(httpClientSpy.get).toBeCalledTimes(1);
    // expect(httpClientSpy.get).toHaveBeenCalledWith(url, {"withCredentials": true});
    // console.log("test end!!!")
  })
});
