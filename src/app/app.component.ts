//@ts-nocheck
import { Component, OnInit, OnChanges } from '@angular/core';
import { AuthService } from './service/auth.service';
import { ShoppingCartService } from './service/shopping-cart.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnChanges{

  public totalItem: number = 0;
  public productList: any;
  admin: boolean;

  constructor(private shoppingCartService: ShoppingCartService, private authService: AuthService){}
  ngOnInit(): void {
    this.shoppingCartService.getProduct()
    .subscribe(res =>{
      this.totalItem = res.length;
      
    })
    this.authService.Admin.subscribe(res => {
      this.admin = res;
    });
    
  }
  title = 'eCommerceProject';
}



