import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import { Observable } from "rxjs";
import { AuthService } from "./service/auth.service";

@Injectable()
export class AdminGuard implements CanActivate{
    constructor(
        private router: Router,
        private authService: AuthService
    ){}

    canActivate(
        route: ActivatedRouteSnapshot, 
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean{
            return this.authService.Admin.pipe((isAdmin) => {
                if(!isAdmin){
                    this.router.navigate(['/product']);
                }
                return isAdmin;
            });
        
    }
}